TARGET_POD=$(kubectl get pods -n bss -o name | grep accounts) && TARGET_POD="${TARGET_POD:4}"
kubectl cp TMF622.jmx $TARGET_POD:/tmp -n bss
kubectl cp jmeter-plugins-casutg-2.10.jar $TARGET_POD:/tmp -n bss
kubectl cp jmeter-plugins-manager-1.7.jar $TARGET_POD:/tmp -n bss
kubectl exec -it $TARGET_POD -n bss /bin/bash 




cd /tmp
curl https://dlcdn.apache.org//jmeter/binaries/apache-jmeter-5.5.tgz -o jmeter.tgz
tar -xzf jmeter.tgz
mkdir -p /tmp/perf
mkdir -p /tmp/salida
mv TMF622.jmx perf
mv jmeter-plugins-manager-1.7.jar /tmp/apache-jmeter-5.5/lib/ext
mv jmeter-plugins-casutg-2.10.jar /tmp/apache-jmeter-5.5/lib/ext
cd apache-jmeter-5.5/bin
./jmeter -n -t /tmp/perf/TMF622.jmx -l /tmp/salida/TMF622-results.jtl -e -o /tmp/salida
